<?php


namespace Bb24\Memwatcher;

/**
 * Class Profiler
 *
 * @author  l.brinkmann
 *
 * @package Bb24\Memwatcher
 */
class Profiler
{
    /** @var bool $initialized */
    protected static $initialized      = false;

    /** @var float|int $faktorGiga */
    protected static $faktorGiga = 1024 * 1024 * 1024;

    /** @var float|int $faktorMega */
    protected static $faktorMega = 1024 * 1024;

    /** @var int $faktorKilo */
    protected static $faktorKilo = 1024;


    /** @var bool $colors */
    protected static $colors       = false;



    /**
     * @author l.brinkmann
     *
     */
    public static function info()
    {
        if(!self::$initialized)
            self::initialize();

        $iMemUsage         = memory_get_usage();
        $iMemPeakUsage     = memory_get_peak_usage();

        $sMemUsage         = self::format($iMemUsage);
        $sMemPeakUsage     = self::format($iMemPeakUsage);

        if(self::$colors)
            echo "\033[1;33m#Memory:\033[0m \033[0;32m{$sMemUsage}\033[0m  \033[1;33m#Peak:\033[0m \033[1;31m{$sMemPeakUsage}\033[0m" . PHP_EOL;
        else
            echo "#Memory: {$sMemUsage}  #Peak: {$sMemPeakUsage}" . PHP_EOL;
    }



    /**
     * @author l.brinkmann
     *
     * @param $iBytes
     *
     * @return string
     */
    protected static function format($iBytes)
    {

        if (($iNormalized = $iBytes / self::$faktorGiga) > 1) {     // GB
            $sUnit = 'GB';
        } elseif (($iNormalized = $iBytes / self::$faktorMega) > 1) { // MB
            $sUnit = 'MB';
        } elseif (($iNormalized = $iBytes / self::$faktorKilo) > 1) { // KB
            $sUnit = 'KB';
        } else {
            $sUnit       = 'B';
            $iNormalized = $iBytes;
        }


        $iNormalized = round($iNormalized, 2);
        $sFormatted  = "{$iNormalized} {$sUnit}";

        return $sFormatted;
    }



    private static function initialize()
    {
        self::$colors   = (PHP_OS_FAMILY != 'Windows' && PHP_SAPI == 'cli');

        self::$initialized  = true;
    }
}
